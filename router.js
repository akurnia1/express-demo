const express = require("express");

const router = express.Router();

router.use(function timeLog(req, res, next) {
  console.log("Time:  ", Date.now());
  next();
});

router.get("/", function (req, res) {
  res.send("Home Page");
});
// router.get("/profile", function (req, res) {
//   res.send("Profile Page");
// });

module.exports = router;
