const express = require("express");
const app = express();
var bodyParser = require("body-parser");
const fs = require("fs");
const morgan = require("morgan");

app.use(bodyParser.urlencoded({ extended: false }));
// parse application/json
app.use(bodyParser.json());
app.set("view engine", "ejs");
let statusUser = "student";
const logger = (req, res, next) => {
  console.log(req.method, req.url);
  next();
};
app.use(logger);
app.use(morgan("dev"));

// // route-level middleware
// app.get("/endpointLain", (req, res) => {
//   res.send("endpont lain");
// });
// app.get("/endpointLain", (req, res) => {
//   res.send("endpont lain");
// });
// const router = require("./router");
// app.use(router);

app.get("/", (req, res) => {
  const nama = "Arief";
  res.render("index", {
    nama: nama,
    kelas: "FSW 23",
  });
});

// app.get("/greet", (req, res) => {
//   const nama = "Arief";
//   console.log("req.query", req.query.kelas);
//   res.render("index", {
//     nama: req.query.nama,
//     kelas: "FSW 23",
//   });
// });

// app.get("/student/:id", function (req, res) {
//   if (
//     statusUser === "student" ||
//     statusUser === "teacher" ||
//     statusUser === "kepala sekolah"
//   ) {
//     fs.readFile("./students.json", "utf-8", (err, data) => {
//       console.log("data", data);
//       let student = JSON.parse(data);
//       res.render("index", { student: student[2] });
//       //   res.status(200).json({
//       //     data: JSON.parse(data),
//       //   });
//     });
//   } else {
//     res.status(200).json({
//       message: "Kamu tidak punya role untuk mengakses halaman ini",
//     });
//   }
// });
app.get("/register", function (req, res) {
  console.log("req.body", req.body);
  res.render("register");
});

// ini akan di hit dari form submit register
app.post("/register", function (req, res) {
  const email = req.body.email;
  console.log("get the email", email);
  res.redirect("/");
});

app.get("/iniError", (req, res) => {
  iniError;
});

app.use((err, req, res, next) => {
  console.error("error", err);
  res.status(500).json({
    status: "fail",
    errors: err.message,
  });
});

app.use((err, req, res, next) => {
  console.error("error", err);
  res.status(404).json({
    status: "not found",
    errors: err.message,
  });
});

app.listen(3000, () => {
  console.log("listening on port...", 3000);
});

// get;
// put;
// post;
// delete app.get("/profile");
// app.post("/add-item");
// app.put("/edit-item/3");
// app.delete("/delete-item/2");

// server biasanya 3000
// frontend server biasanya 8080/8000
