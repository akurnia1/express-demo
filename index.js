const express = require("express");
const app = express();
var bodyParser = require("body-parser");
const fs = require("fs");
const morgan = require("morgan");

app.use(bodyParser.urlencoded({ extended: false }));
// parse application/json
app.use(bodyParser.json());
app.set("view engine", "ejs");
app.use(morgan("dev"));

app.get("/", function (req, res) {
  fs.readFile("./students.json", "utf-8", (err, data) => {
    // ini kalo pake ejs
    res.render("index", {
      students: JSON.parse(data),
    });
    // // ini kalo pake htmlk
    // res.json({
    //   data: JSON.parse(data),
    // });
  });
});

// ini akan di hit dari form submit add
app.post("/add", function (req, res) {
  fs.readFile("./students.json", "utf-8", (err, data) => {
    const nama = req.body.nama;
    let dataStudent = JSON.parse(data);
    dataStudent.push({
      id: dataStudent[dataStudent.length - 1]["id"] + 1,
      nama: nama,
      kelas: "FSW 23",
    });
    fs.writeFile("./students.json", JSON.stringify(dataStudent), (err) => {
      console.log("data student setelah di add", dataStudent);
      res.redirect("/");
    });
  });
});

app.get("/delete/:id", (req, res) => {
  const idToDeleted = req.params.id;
  console.log("idToDeleted", idToDeleted);
  fs.readFile("./students.json", "utf-8", (err, data) => {
    let dataStudent = JSON.parse(data);
    let filtered = dataStudent.filter((stud) => {
      return stud.id !== Number(idToDeleted);
    });
    fs.writeFile("./students.json", JSON.stringify(filtered), (err) => {
      res.redirect("/");
    });
  });
});

app.use((err, req, res, next) => {
  console.error("error", err);
  res.status(500).json({
    status: "fail",
    errors: err.message,
  });
});

app.use((err, req, res, next) => {
  console.error("error", err);
  res.status(404).json({
    status: "not found",
    errors: err.message,
  });
});

app.listen(3000, () => {
  console.log("listening on port...", 3000);
});

// get;
// put;
// post;
// delete app.get("/profile");
// app.post("/add-item");
// app.put("/edit-item/3");
// app.delete("/delete-item/2");

// server biasanya 3000
// frontend server biasanya 8080/8000
